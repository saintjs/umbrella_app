defmodule AppModel.Repo.Migrations.AddApplicantsTable do
  use Ecto.Migration

  def change do
    create table(:applicants) do
      add :refcode,         :string, size: 20
      add :first_name,      :string, size: 30
      add :middle_name,     :string, size: 30
      add :last_name,       :string, size: 30
      add :birth_date,      :string, size: 10
      add :email,           :string, size: 50
      add :place_of_birth,  :string, size: 100
      add :present_add,     :string, size: 100
      add :provincial_add,  :string, size: 100
      add :nationality,     :string, size: 15
      add :gender,          :string, size: 6
      add :status,          :string, size: 10
      add :name_of_spouse,  :string, size: 50
      add :purpose,         :string, size: 10
      add :contact,         :string, size: 20
      add :expiration_date, :utc_datetime
      timestamps()
    end
  end
end
