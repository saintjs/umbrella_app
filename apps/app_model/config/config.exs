use Mix.Config

config :app_model, ecto_repos: [AppModel.Repo]

import_config "#{Mix.env}.exs"
