use Mix.Config

# Configure your database
config :app_model, AppModel.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "app_model_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
