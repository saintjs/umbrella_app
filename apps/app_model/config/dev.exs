use Mix.Config

# Configure your database
config :app_model, AppModel.Repo,
  adapter: Ecto.Adapters.MySQL,
  username: "root",
  password: "",
  database: "app_model_dev",
  hostname: "localhost",
  pool_size: 10
