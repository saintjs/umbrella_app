defmodule AppModel.Application do
  @moduledoc """
  The AppModel Application Service.

  The app_model system business domain lives in this application.

  Exposes API to clients such as the `AppModelWeb` application
  for use in channels, controllers, and elsewhere.
  """
  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    Supervisor.start_link([
      supervisor(AppModel.Repo, []),
    ], strategy: :one_for_one, name: AppModel.Supervisor)
  end
end
