defmodule AppModel.UserQueries do
  import Ecto.Query
  alias AppModel.{Repo, User}

  def add(user) do
    Repo.insert(user)
  end

  def get_by_email(email) do
    Repo.get_by(User, email: email)
  end

  def get_by_id(id) do
    Repo.get(User, id)
  end
end
