defmodule AppModel.User do
  use Ecto.Schema
  import Ecto.Changeset

  alias AppModel.UserQueries

  schema "users" do
    field :email, :string
    field :encrypted_password, :string
    field :password, :string, virtual: true
    field :role, :string

    timestamps()
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:email, :password])
    |> encrypt_password
  end

  defp encrypt_password(changeset) do
    password = get_change(changeset, :password)
    encrypted_password = Comeonin.Pbkdf2.hashpwsalt(password)
    put_change(changeset, :encrypted_password, encrypted_password)
  end

  def valid_authentication?(email, password) do
    case UserQueries.get_by_email(email) do
      nil ->
        {Comeonin.Pbkdf2.dummy_checkpw(), nil}
      user ->
        {Comeonin.Pbkdf2.checkpw(password, user.encrypted_password), user}
    end
  end
end
