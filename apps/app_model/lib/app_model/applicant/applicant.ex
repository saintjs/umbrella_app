defmodule AppModel.Applicant do
  use Ecto.Schema
  import Ecto.Changeset

  schema "applicants" do
    field :refcode,         :string
    field :first_name,      :string
    field :middle_name,     :string
    field :last_name,       :string
    field :birth_date,      :string
    field :email,           :string
    field :place_of_birth,  :string
    field :present_add,     :string
    field :provincial_add,  :string
    field :nationality,     :string
    field :gender,          :string
    field :status,          :string
    field :name_of_spouse,  :string
    field :purpose,         :string
    field :contact,         :string
    field :expiration_date, :naive_datetime
    timestamps()
  end

  def changeset(applicant, params \\ %{}) do
    applicant
    |> cast(params, [:refcode, :first_name, :middle_name, :last_name, :birth_date, :email, :place_of_birth, :present_add, :provincial_add, :nationality, :gender, :status, :name_of_spouse, :purpose, :contact])
    |> validate_required([:refcode, :first_name, :middle_name, :last_name, :birth_date, :email, :place_of_birth, :present_add, :provincial_add, :nationality, :gender, :status, :name_of_spouse, :purpose, :contact])
    |> validate_length(:first_name, min: 2, max: 30)
    |> validate_length(:middle_name, min: 2, max: 30)
  end
end
