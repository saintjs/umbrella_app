defmodule AppModel.ApplicantQueries do
  import Ecto.Query

  alias AppModel.{Repo, Applicant}

  def create(applicant) do
    Repo.insert(applicant)
  end

  def get_by_refcode(refcode) do
    query = from e in Applicant,
      where: e.refcode == ^refcode
    Repo.all(query)
  end
end
