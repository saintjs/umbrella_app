defmodule AppClient.Router do
  use AppClient, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug AppClient.LoadCurrentUser
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", AppClient do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    get "/register", RegistrationController, :new
    post "/register", RegistrationController, :create

    get "/login", SessionController, :index
    post "/login", SessionController, :new
  end

  # Other scopes may use custom stacks.
  # scope "/api", AppClient do
  #   pipe_through :api
  # end
end
