defmodule AppClient.LoadCurrentUser do
  def init(opts) do
    opts
  end
  def call(conn, opts) do
    user_id = Plug.Conn.get_session(conn, :user_id)
    if user_id do
      user = AppModel.UserQueries.get_by_id(user_id)
      Plug.Conn.assign(conn, :current_user, user)
    else
      conn
    end
  end
end
