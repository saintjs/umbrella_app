defmodule AppClient.PageController do
  use AppClient, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
