defmodule AppClient.RegistrationController do
  use AppClient, :controller

  alias AppModel.{User, UserQueries}
  def new(conn, _) do
    render conn, :new
  end

  def create(conn, %{"register" => register_params}) do
    changeset = User.changeset(%User{}, register_params)

    case UserQueries.add(changeset) do
      {:ok, register_params} ->
        redirect(conn, to: "/")
      {:error, changeset} ->
        render conn, :new
    end
    render conn, :new
  end
end
