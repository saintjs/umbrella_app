defmodule AppClient.SessionController do
  use AppClient, :controller

  alias AppModel.User
  def index(conn, _) do
    render conn, :new
  end

  def new(conn, %{"login"=> %{"email" => email, "password" => password}}) do
    {valid, user} = User.valid_authentication?(email, password)
    if valid do
      conn
      |> put_session(:user_id, user.id)
      |> redirect(to: "/")
    else
      render conn, :new
    end
  end
end
