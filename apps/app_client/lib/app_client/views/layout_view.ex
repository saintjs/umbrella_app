defmodule AppClient.LayoutView do
  use AppClient, :view

  def current_user(conn) do
    current_user = conn.assigns[:current_user]
    if current_user do
      current_user.email
    else
      "Anonymous"
    end
  end
end
