# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :app_admin,
  namespace: AppAdmin,
  ecto_repos: [AppAdmin.Repo]

# Configures the endpoint
config :app_admin, AppAdmin.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "MEi9nMdYXrwYYV9B5zWFzydb+u6pbuSuVMZQgNuqY7DQ/1Ze+E1RZGlKBbe3wJCU",
  render_errors: [view: AppAdmin.ErrorView, accepts: ~w(html json)],
  pubsub: [name: AppAdmin.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :app_admin, :generators,
  context_app: false

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
