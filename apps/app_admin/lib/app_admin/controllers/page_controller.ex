defmodule AppAdmin.PageController do
  use AppAdmin, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
