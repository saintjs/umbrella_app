# Umbrella App (1-model, 2-web)

To start your Phoenix server:
  * `mix deps.get`
  * cd apps/ && mix ecto.create
  * cd .. && mix phx.server
  * app_client: port=4000, app_admin: port=4444


What's inside  
  * 1 Ecto App (Model)
  * 1 Web for Client
  * 1 Web for Admin
